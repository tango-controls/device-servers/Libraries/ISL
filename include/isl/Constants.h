//*******************************************************************************
//* Copyright (c) 2004-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
#ifndef __INCLUDE_CONSTANTS_H__
#define __INCLUDE_CONSTANTS_H__


namespace isl {

  const double PI = 3.1415926535897932384626433832795f;

}

#endif
