namespace isl {

ISL_INLINE
Filter::operator Filter*() const
{
  return (Filter*)this;
}

}
